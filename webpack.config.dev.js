var path = require("path");
var webpackMerge = require("webpack-merge");

var webpack = require('webpack');
var commonConfig = require("./webpack.config.common.js");

/**
 * Webpack Constants
 */
const ENV = (process.env.ENV = process.env.NODE_ENV = "development");
const API_URL = (process.env.API_URL = "localhost");
const METADATA = webpackMerge(commonConfig.metadata, {
  host: "localhost",
  API_URL: API_URL,
  port: process.env.port,
  ENV: ENV,
});

module.exports = webpackMerge(commonConfig, {
  devtool: "cheap-module-eval-source-map",

  output: {
    path: path.resolve(__dirname + "/public/js/app"),
    publicPath: "/js/app/",
    filename: "bundle.js",
    chunkFilename: "[id].chunk.js"
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        use: [
          {
            loader: "awesome-typescript-loader",
            options: {
              transpileOnly: true
            }
          },
          { loader: "angular2-template-loader" },
          { loader: "angular-router-loader" }
        ]
      }
    ]
  },
  plugins: [

    new webpack.DefinePlugin({
      ENV: JSON.stringify(METADATA.ENV),
      API_URL: JSON.stringify(METADATA.API_URL),
    //   HMR: METADATA.HMR,
      "process.env": {
        ENV: JSON.stringify(METADATA.ENV),
        NODE_ENV: JSON.stringify(METADATA.ENV),
        // HMR: METADATA.HMR,
        API_URL: JSON.stringify(METADATA.API_URL)
      }
    })
  ]
});
