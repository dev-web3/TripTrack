var path = require('path');

var webpack = require('webpack');
var webpackMerge = require('webpack-merge');
var commonConfig = require('./webpack.config.common.js');

const ENV = (process.env.ENV = process.env.NODE_ENV = 'production');
const API_URL = (process.env.API_URL = "triptrackapp.herokuapp.com");
const METADATA = webpackMerge(commonConfig.metadata, {
  host: 'localhost',
  API_URL: API_URL,
  port: 8080,
  ENV: ENV,
});


module.exports = webpackMerge.smart(commonConfig, {
    entry: {
        'app': './assets/app/main.aot.ts'
    },

    output: {
        path: path.resolve(__dirname + '/public/js/app'),
        filename: 'bundle.js',
        publicPath: '/js/app/',
        chunkFilename: '[id].[hash].chunk.js'
    },

    module: {
        rules: [
            {
                test: /\.ts$/,
                use: [
                    'awesome-typescript-loader',
                    'angular2-template-loader',
                    'angular-router-loader?aot=true'
                ]
            }
        ]
    },

    plugins: [
        new webpack.optimize.UglifyJsPlugin({
            sourceMap: false
        }),
        new webpack.DefinePlugin({
            ENV: JSON.stringify(METADATA.ENV),
            API_URL: JSON.stringify(METADATA.API_URL),
            "process.env": {
              ENV: JSON.stringify(METADATA.ENV),
              NODE_ENV: JSON.stringify(METADATA.ENV),
              API_URL: JSON.stringify(METADATA.API_URL)
            }
          })
    ]
});