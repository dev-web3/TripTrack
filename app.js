var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var passport = require('passport');
var expressSession = require('express-session');

var mongoose = require('mongoose');

var User = require('./models/user');
var RememberMeToken = require('./models/rememberMeToken');

// Routes
var appRoutes = require('./routes/app');

var travelsRoutes = require('./routes/travels');
var mapsRoutes = require('./routes/maps');

var app = express();

process.env['NODE_ENV'] = app.get('env');
//mongodb://main:44iENj2i@ds117965.mlab.com:17965/triptrackappdb
//'mongodb://localhost:27017/triptrackappdb'

var dbConnexion = process.env.MONGODB_URI || 'mongodb://localhost:27017/triptrackappdb';
mongoose.Promise = require('bluebird');
mongoose.connect(dbConnexion, {useMongoClient: true /*, server: { poolSize: 5}*/});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(express.static(path.join(__dirname, 'public')));
app.use(cookieParser(process.env.JWT_SECRET || 'ThisIsSoFuckinSecretBro'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(expressSession({
    saveUninitialized: true,
    resave: false,
    // store: sessionStorage,
    secret: process.env.JWT_SECRET || 'ThisIsSoFuckinSecretBro',
    cookie: {
        // path: '/',
        // httpOnly: true,
        secure: false,
        maxAge: 7 * 24 * 60 * 60 * 1000 // 1 wekk
    }
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(function (req, res, next) {
    passport.authenticate("remember-me", function (err, user, info) {
        if (!user) {
            res.redirect("/");
        }
        req.logIn(user, function (err) {
            if (err) {
                return res.status(500).json({
                    message: "Server internal error !"
                });
            }
            user.password = "";
            RememberMeToken.issueToken(user, function (err, rememberToken) {
                if (err) {
                    return res.status(500).json({
                        message: "Server internal error !"
                    });
                }
                res.cookie("remember_me", rememberToken.token, {
                    /*path: '/', httpOnly: true, */ maxAge: 604800000
                }); // 7 days
                var jwtToken = User.generateToken(user);
                return res.redirect("/auth/success/" + jwtToken);
            });
        });
    })(req, res, next);
});

app.use(passport.authenticate("remember-me"));

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Methods', 'POST, GET, PATCH, DELETE, OPTIONS');
    next();
});

// Initialisze Passport
var initPassport = require('./passport/init');
initPassport(passport);

// Routes for user request
var authRoutes = require('./routes/auth')(passport);
var accountRoutes = require('./routes/account')(passport);
app.use('/auth', authRoutes);

app.use('/account', accountRoutes);
app.use('/maps', mapsRoutes);
app.use('/back_travels', travelsRoutes);
app.use('/', appRoutes);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    return res.render('index');
});

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

module.exports = app;
