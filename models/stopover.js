var mongoose = require('mongoose');
var Schema = mongoose.Schema;




var schema = new Schema({
    city: {type: String, required: true},
    position: {type: Number, required: true},
    user: {type: Schema.Types.ObjectId, ref: 'User'},
    //travel: {type: Schema.Types.ObjectId, ref: 'Travel'},
    latitude: {type: Number, required: true},
    longitude: {type: Number, required: true}
});

module.exports = mongoose.model('Stopover', schema);