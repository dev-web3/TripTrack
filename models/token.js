var mongoose = require("mongoose");
var Schema = mongoose.Schema;
const _ = require("underscore");
var mongooseUniqueValidator = require("mongoose-unique-validator");

const tokenSchema = new mongoose.Schema({
  _userId: { type: mongoose.Schema.Types.ObjectId, required: true, ref: "User" },
  token: { type: String, required: true, index: { unique: true } },
  date_created: {
    type: Date,
    required: true,
    default: Date.now,
    expires: 43200
  }
});

tokenSchema.plugin(mongooseUniqueValidator);

module.exports = mongoose.model("Token", tokenSchema);

