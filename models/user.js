var mongoose = require("mongoose");
var Schema = mongoose.Schema;
const _ = require("underscore");
var jwt = require("jsonwebtoken");
var mongooseUniqueValidator = require("mongoose-unique-validator");
var passportLocalMongoose = require("passport-local-mongoose");

var userSchema = new Schema({
  provider: {
    type: String,
    reqired: true,
    enum: ["local", "facebook", "google"]
  },
  displayName: { type: String, default: null },
  username: { type: String, default: null },
  name: {
    familyName: { type: String, require: true },
    givenName: { type: String, require: true },
    middleName: { type: String, default: null }
  },
  emails: [
    {
      value: { type: String, lowercase: true },
      type: { type: String, enum: ["local", "facebook", "google"] }
    }
  ],
  password: { type: String, default: null },
  local: {
    id: { type: String, default: null },
    token: { type: String, default: null }
  },
  facebook: {
    id: { type: String, default: null },
    token: { type: String, default: null }
  },
  google: {
    id: { type: String, default: null },
    token: { type: String, default: null }
  },
  description: { type: String, default: null },
  webUrl: { type: String, default: null },
  nationality: { type: String, default: null },
  date_created: { type: Date, default: Date.now },
  isVerified: { type: Boolean, default: false },
  photos: [
    { value: String }
  ]
});

userSchema.plugin(mongooseUniqueValidator);
// schema.plugin(passportLocalMongoose, {
//     usernameField:  'emails.value',
// });

userSchema.statics.generateToken = function(user){
  var token = jwt.sign(
    { user: user },
    process.env.JWT_SECRET || "ThisIsSoFuckinSecretBro",
    { expiresIn: 7 * 24 * 60 * 60 * 1000 /*1 week */ }
  );
  return token;
};

module.exports = mongoose.model("User", userSchema);
