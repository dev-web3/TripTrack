var mongoose = require('mongoose'), Stopover = require('./stopover.js'),
StopoverSchema = mongoose.model('Stopover').schema,
Schema = mongoose.Schema;



var schema = new Schema({
    travelName: {type: String, required: true}, 
    startDate:{type:Date,required: true},
    travelMode:{type : String, required : true},
    users: [{type: Schema.Types.ObjectId, ref: 'User'}],
    stopovers: [StopoverSchema]
});

module.exports = mongoose.model('Travel', schema);