var mongoose = require("mongoose");
var Schema = mongoose.Schema;
const _ = require("underscore");
var crypto = require("crypto");
var mongooseUniqueValidator = require("mongoose-unique-validator");

const rememberMeTokenSchema = new mongoose.Schema({
	_userId: {
		type: mongoose.Schema.Types.ObjectId,
		required: true,
		ref: "User"
	},
	token: { type: String, required: true /*, index: { unique: true }*/ },
	date_created: {
		type: Date,
		required: true,
		default: Date.now,
		expires: 604800000
	}
});

rememberMeTokenSchema.plugin(mongooseUniqueValidator);

rememberMeTokenSchema.statics.consumeRememberMeToken = function (token, fn) {

	this.model('RememberMeToken').findOne({ 'token': token }, function (err, token) {
		if (!token) {
			return fn(null, null);
		}
		var _userId = token._userId;
		token.remove(function (err) {
			if (err) {
				return fn(err);
			}
			return fn(null, _userId);
		});
	});
};

rememberMeTokenSchema.statics.issueToken = function (user, done) {
	var RememberMeToken = this.model("RememberMeToken", rememberMeTokenSchema);
	var token = new RememberMeToken({
		_userId: user._id,
		token: crypto.randomBytes(64).toString("hex")
	});
	token.save(function(err) {
		if (err) {
		  return done(err);
		}
		return done(null, token);
	});
};

module.exports = mongoose.model("RememberMeToken", rememberMeTokenSchema);
