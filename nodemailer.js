const nodemailer = require("nodemailer");
var nodemailerConfig = require("./nodemailer-config");
var xoauth2 = require("xoauth2");
var mailConfig;

if (process.env.NODE_ENV === "production") {

  mailConfig = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
      user: process.env.EMAIL_USERNAME,
      pass: process.env.EMAIL_PASSWORD
    }
  });

} else {
  
  mailConfig = nodemailer.createTransport({
    host: "smtp.ethereal.email",
    port: 587,
    secure: false, // true for 465, false for other ports
    auth: {
      user: nodemailerConfig.ethereal.user, // generated ethereal user
      pass: nodemailerConfig.ethereal.password // generated ethereal password
    }
  });

}

module.exports = {
  nodemailer: nodemailer,
  transporter: mailConfig
};
