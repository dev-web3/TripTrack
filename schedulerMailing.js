var cron = require('node-schedule');
//for making promises
var Q = require('q');
var User = require("./models/user");
var Travel = require('./models/travel');
var nodemailer = require("./nodemailer");
var mailScheduler = require('./schedulerMailing');

// method to format the mail optionsgit
function userMailFormat(userTravel) {
    var name = userTravel.name;
    var email = userTravel.email;
    var startDate = userTravel.date;
    var travelName = userTravel.travelName;

    var mailOptions = {
        from: process.env.EMAIL_USERNAME || "no-reply@codemoto.io",
        to: email,
        subject: "TripTrack Journey Reminder !!! ",
        text:
            "Hello " + name + ",\n\n" +
            "Trip track wanted to remind you that you will start your journey " + travelName + " on " + startDate + ". \n Thank You, \n Triptrack team."
    };

    return mailOptions;
}
//a promise that will send mail for a user
var mailSender = function (userTravel) {
    //set up the promise
    var deferred = Q.defer();
    nodemailer.transporter.sendMail(userMailFormat(userTravel), (err, info) => {
        if (err) {
            deferred.reject(console.log('sending mail failed: ' + err));;
        }
        else {
            if (process.env.NODE_ENV === "development") {
                console.log("Message sent: %s", info.messageId);
                // Preview only available when sending through an Ethereal account
                console.log(
                    "Preview URL: %s",
                    nodemailer.nodemailer.getTestMessageUrl(info)
                );
            }

            deferred.resolve(console.log("sending mail successful"));
        }
    });
    return deferred.promise;
}

let initScheduler = function (mailDate, userTravel) {
    cron.scheduleJob(mailDate, function () {
        mailSender(userTravel)
    });
}

module.exports = {
    initScheduler: initScheduler
}