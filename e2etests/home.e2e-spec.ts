//import { AngularE2eTestingPage } from './home.po';
import { browser,by, element,Key  } from 'protractor';
/*
describe('Creating new trip route', () => {
  it('should connect as user', () => {
  });
}); */




describe('Creating new trip route', () => {
    let page;
 
    it('should connect as user', () => {
      //ssdds
      //Connexion a la page
      page = browser.get('http://localhost:3000');//Rajouter l'addresse heroku et options?
      browser.ignoreSynchronization = true;
      
    
      var email = element(by.id('email'));
      var password = element(by.id('password'));
      
      browser.sleep(500);
      email.sendKeys('triptrackfake@gmail.com');
      //console.log('hole');
      browser.sleep(500);
      password.sendKeys('test');
      browser.sleep(500);

      var buttonSubmit = element(by.id('submit'));
      buttonSubmit.click();

      //Connected on /travels
      browser.sleep(1000);

    });
    
    it('should create a travel', () => {
      
      var buttonCreateTravel = element(by.id('add-travel-btn'));
      buttonCreateTravel.click();

      var startDate = element(by.id('startDate'));
      var travelName = element(by.id('travelName'));
      //var walking = element(by.id('walking'));
      var source = element(by.id('source'));
      var destination = element(by.id('destination'));
      


      startDate.sendKeys('10-12-2017');
      startDate.sendKeys(Key.TAB);
      browser.sleep(500);
      travelName.sendKeys('Voyage test');
      travelName.sendKeys(Key.TAB);
      browser.sleep(500);
      //element(by.id('walking')).click();
      //element(by.id('source')).sendKeys('Madrid, Espagne');
      //element(by.id('destination')).sendKeys('Rome, Italie');
      //browser.sleep(10000);
      //element(by.id('showMap')).click();

      source.sendKeys('Madrid');
      browser.driver.actions().mouseMove(source);
      browser.sleep(1000);
      source.sendKeys(Key.ARROW_DOWN);
      browser.sleep(500);
      source.sendKeys(Key.ENTER);
      source.sendKeys(Key.TAB);
      browser.sleep(500);
      destination.sendKeys('Rome');
      //browser.driver.actions().mouseMove(source);
      browser.sleep(1000);
      destination.sendKeys(Key.ARROW_DOWN);
      browser.sleep(500);
      destination.sendKeys(Key.ENTER);
      destination.sendKeys(Key.TAB);

      //walking
      browser.sleep(500);
            
      element(by.id('showMap')).click();
      
      browser.sleep(3000);
      element(by.id('onSave')).click();
      browser.sleep(500);
  
      
    });

    it('should go on the newly created trip', () => {
      
            //browser.refresh();
      
            element.all(by.id('listOfTravels')).get(0).click();
            //element.all(by.css('li')).get(1)
            //Bruxelles
            //Paris
            expect(element(by.id('stopoversList')).all(by.css('li')).get(0).getText()).toEqual('Madrid');
            expect(element(by.id('stopoversList')).all(by.css('li')).get(1).getText()).toEqual('Rome');

            browser.sleep(12000);
            

      
          });
});
