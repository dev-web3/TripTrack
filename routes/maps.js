var express = require('express');
var router = express.Router();

var Travel = require('../models/travel');
var Stopover = require('../models/stopover');
var mailScheduler = require('../schedulerMailing');
var isAuthenticated = function (req, res, next) {
    // if user is authenticated in the session, call the next() to call the next request handler
    // Passport adds this method to request object. A middleware is allowed to add properties to
    // request and response objects
    if (req.isAuthenticated()) return next();
    // if the user is not authenticated then redirect him to the login page
    res.redirect("/");
};


router.post('/', isAuthenticated, function (req, res, next) {

    // console.log("truc");
    // console.log("req : " + req);
    // console.log("body : " + req.body);
    // console.log("travelName : " + req.body.travelName);
    /*
        city: {type: String, required: true},
        position: {type: Number, required: true},
        user: {type: Schema.Types.ObjectId, ref: 'User'},
        //travel: {type: Schema.Types.ObjectId, ref: 'Travel'},
        latitude: {type: Number, required: true},
        longitude: {type: Number, required: true}
    
    */
    //var userID = req.user._id

    //console.log("User id : "+userID);
    //findOne user et le add a chaque fois

    let stopovers = [];

    for (let element of req.body.stopovers) {
        // console.log("element : " + element);
        var stopover = new Stopover({
            city: element.city,
            position: element.position,
            latitude: element.latitude,
            longitude: element.longitude,
        })
        stopovers.push(stopover);
        //stopover.save();
    }
    let users = [];
    users.push(req.user._id);

    // console.log("stopovers : " + stopovers);

    var travel = new Travel({
        travelName: req.body.travelName,
        startDate: req.body.startDate,
        travelMode: req.body.travelMode,
        users: users,
        stopovers: stopovers
    });

    travel.save(function (err, result) {
        if (err) {
            return res.status(500).json({
                title: 'An error occured',
                error: err
            });
        }
        res.status(201).json({
            message: 'Your travel has been saved.',
            obj: result
        });
        //schedulerMail(travel, req.user._id);
        // console.log(req.user);
        schedulerMail(travel, req.user);
    });

    var schedulerMail = function (travel, user) {
        var userTravel;

        userTravel = { name: user.name.givenName, email: user.emails[0].value, date: travel.startDate, travelName: travel.travelName };
        var travelDate = new Date(travel.startDate);
        var mailDate = new Date(travelDate);
        // send the mail before 2 days of the start date
        mailDate.setDate(mailDate.getDate() - 2);

        // the date used for test : send a mail 2 minutes after the save
        var TestmailDate = new Date();
        TestmailDate.setMinutes(TestmailDate.getMinutes() + 2)
        mailScheduler.initScheduler(TestmailDate, userTravel);
    }

});

module.exports = router;