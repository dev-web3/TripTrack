var express = require('express');
var router = express.Router();

var Travel = require('../models/travel');

//req.user._id
var isAuthenticated = function (req, res, next) {
	// if user is authenticated in the session, call the next() to call the next request handler
	// Passport adds this method to request object. A middleware is allowed to add properties to
	// request and response objects
	if (req.isAuthenticated()) return next();
	// if the user is not authenticated then redirect him to the login page
	res.redirect("/");
};

router.get('/',isAuthenticated, function (req, res, next){
    //where('likes').in(['vaporizing', 'talking']).
    Travel.find().where('users').in([req.user._id])
    .exec(function(err, travels){
        if(err){
            return res.status(500).json({
                title: 'An error occured while We try to find your travels.',
                error: err
            });
        }
        res.status(200).json({
            message: 'Success Travel.find()',
            obj: travels
        });
    });
});

module.exports = router;