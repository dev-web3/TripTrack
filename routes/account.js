var express = require("express");
var router = express.Router();
var crypto = require("crypto");
var bCrypt = require("bcrypt");
var nodemailer = require("../nodemailer");

var User = require("../models/user");
var Token = require("../models/token");

var ForgotToken = require("../models/forgotToken");

var isAuthenticated = function(req, res, next) {
  // if user is authenticated in the session, call the next() to call the next request handler
  // Passport adds this method to request object. A middleware is allowed to add properties to
  // request and response objects;
  if (req.isAuthenticated()) return next();
  // if the user is not authenticated then redirect him to the login page
  res.redirect("/");
};

module.exports = function(passport) {
  router.get("/", isAuthenticated, function(req, res, next) {
    console.log("get - /user/");
    User.findOne({ _id: req.user._id }, function(err, user) {
      if (err) {
        console.log("Error in retrieve user profile");
        // renvoie error
      }
      if (!user) {
      }
      user.password = "";
      res.status(200).json({
        message: "Success",
        obj: user
      });
    });
  });

  router.patch("/update", isAuthenticated, function(req, res, next) {
    User.findOneAndUpdate(
      { _id: req.user._id },
      {
        $set: {
          username: req.body.username,
          displayName: req.body.givenName + " " + req.body.familyName,
          name: {
            familyName: req.body.familyName,
            givenName: req.body.givenName
          },
          nationality: req.body.nationality,
          description: req.body.description,
          webUrl: req.body.webUrl
        }
      },
      { new: true },
      function(err, user) {
        if (err) {
          return res.status(500).json({
            message: "An error occured, try again later.",
            error: err
          });
        }
        user.password = "";
        res.status(200).json({
          message: "Your profile has been updated",
          obj: user
        });
      }
    );
  });

  router.get("/verify/:token", function(req, res, next) {
    Token.findOne({ token: req.params.token }, function(err, token) {
      if (err) {
        return res.status(500).json({
          message: "An error occured, try again later"
        });
      }
      if (!token) {
        return res.status(400).json({
          type: "not-verified",
          message:
            "We were unable to find a valid token. Your token my have expired."
        });
      }

      User.findOne({ _id: token._userId }, function(err, user) {
        if (err) {
          return res.status(500).json({
            message: "An error occured"
          });
        }
        if (!user) {
          return res.status(400).json({
            message: "We were unable to find a user for this token."
          });
        }
        if (user.isVerified) {
          return res.status(400).json({
            type: "already-verified",
            message: "This user has already been verified."
          });
        }

        User.findOneAndUpdate(
          { _id: user._id },
          { $set: { isVerified: true } },
          { new: true },
          function(err, user) {
            if (err) {
              return res.status(500).json({ msg: err.message });
            }
            user.password = "";
            res.status(200).json({
              message: "The account has been verified. Please log in.",
              obj: user
            });
          }
        );
      });
    });
  });

  router.post("/resend", function(req, res, next) {
    User.findOne({ "emails.value": req.body.email }, function(err, user) {
      if (err) {
        return res.status(500).json({
          message: "An error occured. Please try again later."
        });
      }
      if (!user) {
        return res.status(400).json({
          message:
            "We were unable to find a user with this email. Please login first."
        });
      }
      if (user.isVerified) {
        return res.status(400).json({
          message: "This account has already been verified. Please log in."
        });
      }

      var token = new Token({
        _userId: user._id,
        token: crypto.randomBytes(64).toString("hex")
      });

      token.save(function(err) {
        if (err) {
          return res.status(500).json({
            message: "An error occured."
          });
        }

        var mailOptions = {
          from: process.env.EMAIL_USERNAME || "no-reply@codemoto.io",
          to: user.emails[0].value,
          subject: "Account Verification Token",
          text:
            "Hello,\n\n" +
            "Please verify your account by clicking the link: \nhttps://" +
            req.headers.host +
            "/user/verify/confirm/" +
            token.token +
            "\n"
        };
        nodemailer.transporter.sendMail(mailOptions, (err, info) => {
          if (err) {
            return res.status(500).send({ msg: err.message });
          }
          if (process.env.NODE_ENV === "development") {
            console.log("Message sent: %s", info.messageId);
            // Preview only available when sending through an Ethereal account
            console.log(
              "Preview URL: %s",
              nodemailer.nodemailer.getTestMessageUrl(info)
            );
          }

          res.status(200).json({
            message:
              "A verification email has been sent to " +
              user.emails[0].value +
              "."
          });
        });
      });
    });
  });

  router.post("/forgot", function(req, res, next) {
    User.findOne({ "emails.value": req.body.email }, function(err, user) {
      if (err) {
        return res.status(500).json({
          message: "An error occured. Please try again later."
        });
      }
      if (!user) {
        return res.status(400).json({
          message:
            "We were unable to find a user with this email. Please register first."
        });
      }
      var forgotToken = new ForgotToken({
        _userId: user._id,
        token: crypto.randomBytes(64).toString("hex")
      });
      forgotToken.save(function(err) {
        if (err) {
          return res.status(500).json({
            message: "An error occured. Please try again later."
          });
        }

        var mailOptions = {
          from: process.env.EMAIL_USERNAME || "no-reply@codemoto.io",
          to: user.emails[0].value,
          subject: "Reset your password",
          text:
            "Hello,\n\n" +
            "Please reset the password of your account by clicking the link: \nhttps://" +
            req.headers.host +
            "/user/forgot/reset/" +
            forgotToken.token +
            "\n"
        };

        nodemailer.transporter.sendMail(mailOptions, (err, info) => {
          if (err) {
            return res.status(500).send({ msg: err.message });
          }
          if (process.env.NODE_ENV === "development") {
            console.log("Message sent: %s", info.messageId);
            // Preview only available when sending through an Ethereal account
            console.log(
              "Preview URL: %s",
              nodemailer.nodemailer.getTestMessageUrl(info)
            );
          }
          res.status(200).json({
            message:
              "An email has been sent to " +
              user.emails[0].value +
              " to reset your password."
          });
        });
      });
    });
  });

  router.get("/reset/:token", function(req, res, next) {
    ForgotToken.findOne({ token: req.params.token }, function(err, token) {
      if (err) {
        return res.status(500).json({
          message: "An error occured"
        });
      }
      if (!user) {
        return res.status(400).json({
          message:
            "We were unable to find a user for this token. The token maybe expires."
        });
      }
      res.redirect("/user/forgot/reset/" + req.params.token);
    });
  });

  router.post("/reset/:token", function(req, res, next) {
    ForgotToken.findOne({ token: req.params.token }, function(err, token) {
      if (err) {
        return res.status(500).json({
          message: "An error occured, try again later."
        });
      }
      if (!token) {
        return res.status(400).json({
          message:
            "We were unable to find a request for this token. The token maybe expires."
        });
      }
      User.findOneAndUpdate(
        { _id: token._userId },
        { $set: { password: bCrypt.hashSync(req.body.password, 10) } },
        { new: true },
        function(err, user) {
          if (err) {
            return res.status(500).json({
              message: "An error occured, try again later."
            });
          }
          ForgotToken.remove({ _id: token._id }, function(err) {
            if (err) {
              return res.status(500).json({
                message: "An error occured, try again later."
              });
            }

            var mailOptions = {
              from: process.env.EMAIL_USERNAME || "no-reply@codemoto.io",
              to: user.emails[0].value,
              subject: "Your password has been changed",
              text:
                "Hello,\n\n" +
                "The password of your account has been changed." +
                "\n"
            };
            nodemailer.transporter.sendMail(mailOptions, (err, info) => {
              if (err) {
                return res.status(500).send({ msg: err.message });
              }
              if (process.env.NODE_ENV === "development") {
                console.log("Message sent: %s", info.messageId);
                // Preview only available when sending through an Ethereal account
                console.log(
                  "Preview URL: %s",
                  nodemailer.nodemailer.getTestMessageUrl(info)
                );
              }
              res.status(200).json({
                message: "Your password has been changed."
              });
            });
          });
        }
      );
    });
  });

  return router;
};
