var express = require("express");
var jwt = require("jsonwebtoken");
var router = express.Router();
var crypto = require("crypto");

var RememberMeToken = require("../models/rememberMeToken");
var User = require("../models/user");

var isAuthenticated = function(req, res, next) {
  // if user is authenticated in the session, call the next() to call the next request handler
  // Passport adds this method to request object. A middleware is allowed to add properties to
  // request and response objects
  if (req.isAuthenticated()) return next();
  // if the user is not authenticated then redirect him to the login page
  res.redirect("/");
};

function generateToken(user) {
  var token = jwt.sign(
    { user: user },
    process.env.JWT_SECRET || "ThisIsSoFuckinSecretBro",
    { expiresIn: 7 * 24 * 60 * 60 * 1000 /*1 week */ }
  );
  return token;
}

module.exports = function(passport) {
  /* Handle Login POST */
  router.post("/signin", function(req, res, next) {
    passport.authenticate("login", function(err, user, info) {
      if (err) {
        return next(err);
      }
      if (!user) {
        return res.status(401).json({
          message: "Incorrect email or password."
        });
      }
      if (!user.isVerified) {
        return res.status(401).json({
          message: "Your account has not been verified."
        });
      }
      req.logIn(user, function(err) {
        if (err) {
          return res.status(500).json({
            message: "Could not log in user. Try again later."
          });
        }
        user.password = "";

        // if remeber me is checked
        if (req.body.remember) {

          RememberMeToken.issueToken(user, function(err, rememberToken) {
            if (err) {
              return res.status(500).json({
                message: "Remember me token error.",
                err: err
              });
            }
            res.cookie("remember_me", rememberToken.token, {
              /*path: '/', httpOnly: true, */ maxAge: 604800000
            }); // 7 days
            res.status(200).json({
              message: "Login successful !",
              obj: user,
              jwt: User.generateToken(user)
            });
          });
        } else {
          res.clearCookie("remember_me");
          req.session.cookie.maxAge = false;
          res.status(200).json({
            message: "Login successful !",
            obj: user,
            jwt: User.generateToken(user)
          });
        }
      });
    })(req, res, next);
  });

  /* Handle Registration POST */
  router.post("/signup", function(req, res, next) {
    passport.authenticate("signup", function(err, user, info) {
      if (err) {
        return res.status(500).json({
          message: "An error occured. Try again later.",
          error: err
        });
      }
      if (!user) {
        if(info){
          return res.status(400).json({
            message: info
          });
        }
        return res.status(500).json({
          message: "An error occured"
        });
      }
      res.status(201).json({
        message: "Successful registration ! Please verify your email to login :)",
        obj: user
      });
    })(req, res, next);
  });

  /* Route for Facebook authentification and sign in 
	  */
  router.get("/signin/facebook", function(req, res, next) {
    passport.authenticate("facebook", { scope: "email" }, function(
      err,
      user,
      info
    ) {})(req, res, next);
  });

  router.get("/signin/facebook/callback", function(req, res, next) {
    passport.authenticate("facebook", function(err, user, info) {
      if (err) {
        return next(err);
      }
      if (!user) {
        return res.status(401).json({
          message: info
        });
      }

      req.logIn(user, function(err) {
        if (err) {
          return res.status(500).json({
            message: "Could not log in user. Try again later."
          });
        }

        var jwtToken = User.generateToken(user);
        res.clearCookie("remember_me");
        res.redirect("/auth/success/" + jwtToken);
      });
    })(req, res, next);
  });

  router.get("/signin/google", function(req, res, next) {
    passport.authenticate(
      "google",
      {
        scope: [
          "https://www.googleapis.com/auth/plus.login",
          ,
          "https://www.googleapis.com/auth/plus.profile.emails.read"
        ]
      },
      function(err, user, info) {}
    )(req, res, next);
  });

  router.get("/signin/google/callback", function(req, res, next) {
    passport.authenticate("google", function(err, user, info) {
      if (err) {
        return next(err);
      }
      if (!user) {
        return res.status(401).json({
          message: info
        });
      }

      req.logIn(user, function(err) {
        if (err) {
          return res.status(500).json({
            message: "Could not log in user. Try again later."
          });
        }
        var jwtToken = User.generateToken(user);
        res.clearCookie("remember_me");
        res.redirect("/auth/success/" + jwtToken);
      });
    })(req, res, next);
  });

  /* Handle Logout */
  router.get("/logout", isAuthenticated, function(req, res) {
    res.clearCookie("remember_me");
    req.logout();
    req.session.destroy();
    return res.status(200).json({
		message: "successful disconnection."
	});
  });

  router.get("/isAuthenticated", function(req, res, next) {
    if (req.isAuthenticated()) {
	  var user = req.user;
	  user.password = "";
      return res.status(200).json({
        message: "You're already authenticated",
        obj: User.generateToken(user)
      });
    }
    return res.status(401).json({
      message: "You're not longer authenticated, please sign in again."
    });
  });

  return router;
};
