
exports.config = {

  framework: 'jasmine',  
  capabilities: {
    browserName: 'chrome',
    chromeOptions: {
      args: [ "--headless", "--disable-gpu", "--window-size=800,600" ]
    }
  },  
  allScriptsTimeout: 110000, 
    jasmineNodeOpts: {
      showTiming: true,
      showColors: true,
      isVerbose: false,
      includeStackTrace: false,
      defaultTimeoutInterval: 400000
    },
  specs: ['./e2etests/home.e2e-spec.ts'],
  directConnect: true,
  useAllAngular2AppRoots: true,


  noGlobals: true,
  onPrepare() {
    require('ts-node').register({
      project: './e2etests/tsconfig.e2etest.json'
    });
    
  }

};
