import { Component, OnInit } from "@angular/core";
import { FormGroup, NgForm, Validators, FormControl } from "@angular/forms";

import { UserService } from "../../_services/user.service";
import { AlertService } from "../../_services/alert.service";

@Component({
  selector: "app-user-help-forgot",
  templateUrl: "./forgot.component.html"
})
export class ForgotComponent implements OnInit {

  forgotForm: FormGroup;

  constructor(
    private userService: UserService,
    private alertService: AlertService
  ) {}

  onSubmit() {
    if(this.forgotForm.valid){
      this.userService.forgot(this.forgotForm.value.email).subscribe(
        data => {
          this.alertService.success(data.message);
        },
        error => {
          this.alertService.error(error.message);
        }
      );
    }
  }

  ngOnInit(): void {
    this.forgotForm = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email])
    });
  }
}
