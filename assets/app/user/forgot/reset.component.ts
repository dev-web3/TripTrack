import { Component, OnInit } from "@angular/core";
import {
  FormGroup,
  NgForm,
  Validators,
  FormControl,
  FormBuilder
} from "@angular/forms";
import { ActivatedRoute } from "@angular/router";

import { UserService } from "../../_services/user.service";
import { AlertService } from "../../_services/alert.service";

@Component({
  selector: "app-user-help-reset",
  templateUrl: "./reset.component.html"
})
export class ResetComponent {
  resetForm: FormGroup;
  token: string;
  constructor(
    private route: ActivatedRoute,
    private userService: UserService,
    private alertService: AlertService,
    private formBuilder: FormBuilder
  ) {}

  onSubmit() {
      if(this.resetForm.valid){
        this.userService.reset(this.resetForm.value.password, this.token)
        .subscribe(
          data => {
            this.alertService.success(data.message);
          },
          error => {
            this.alertService.error(error.message);
          }
        );
      }
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.token = params.token;
    });
    this.resetForm = this.formBuilder.group(
      {
        password: new FormControl(null, [Validators.required]),
        newPassword: new FormControl(null, [Validators.required])
      },
      { validator: this.matchPassword('password', 'newPassword') }
    );
  }

  matchPassword(password: string, confirmation: string) {
    return (group: FormGroup) => {
      let passwordInput = group.controls[password],
        passwordConfirmationInput = group.controls[confirmation];
      if (passwordInput.value !== passwordConfirmationInput.value) {
        return passwordConfirmationInput.setErrors({ notEquivalent: true });
      } else {
        return passwordConfirmationInput.setErrors(null);
      }
    };
  }
}
