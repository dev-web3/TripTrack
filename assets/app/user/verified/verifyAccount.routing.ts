import { Routes, RouterModule } from "@angular/router";
import { ConfirmAccountComponent } from "./account/confirmAccount.component";
import { ResendVerificationMailComponent } from "./account/resendVerificationMail.component";

export const VERIFY_ROUTES: Routes = [
    { path: '', redirectTo: 'confirm', pathMatch: 'full'},
    { path: 'confirm/:token', component: ConfirmAccountComponent },
    { path: 'resend', component: ResendVerificationMailComponent }
]