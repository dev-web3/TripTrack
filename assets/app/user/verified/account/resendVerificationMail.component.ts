import { Component, OnInit } from "@angular/core";
import { FormGroup, NgForm, Validators, FormControl } from "@angular/forms";

import { UserService } from "../../../_services/user.service";
import  { AlertService } from "../../../_services/alert.service";

@Component({
  selector: "app-verify-account-resend",
  templateUrl: "./resendVerificationMail.component.html"
})
export class ResendVerificationMailComponent implements OnInit {
  resendForm: FormGroup;

  constructor(private userService: UserService, private alertService: AlertService) {}

  onSubmit() {
    if(this.resendForm.valid){
      this.userService
      .resendVerifyAccountEmail(this.resendForm.value.email)
      .subscribe(data => {
        this.alertService.success(data.message);
      }, error => {
        this.alertService.error(error.message);
      });
    }
  }

  ngOnInit(): void {
    this.resendForm = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email])
    });
  }
}
