import { Component, OnInit, Input } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

import { UserService } from "../../../_services/user.service";
import { AlertService } from "../../../_services/alert.service";

import { UserHelper } from "../../../_helpers/user.helper";

import { User } from "../../../_models/user.model";

@Component({
  selector: "app-verify-account-confirm",
  templateUrl: "./confirmAccount.component.html"
})
export class ConfirmAccountComponent implements OnInit {
  private verifyToken: string;
  @Input() user: User;

  constructor(
    private route: ActivatedRoute,
    private userService: UserService,
    private alertSerice: AlertService,
    private userHelper: UserHelper
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.verifyToken = params.token;
    });

    this.userService.verifyUser(this.verifyToken).subscribe(
      data => {
        this.user = this.userHelper.parseUser(data.obj);
      },
      error => {}
    );
  }

  isVerified(): boolean {
    if (!this.user) return false;
    return this.user.isVerified;
  }
}
