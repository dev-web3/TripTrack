import { Component, Input } from "@angular/core";
import { User } from "../_models/user.model";
import { UserService } from "../_services/user.service";

@Component({
    selector:'app-user',
    templateUrl: './user.component.html'
    // providers: [UserService]
})
export class UserComponent{
    
    constructor(private userService: UserService){}
       
}