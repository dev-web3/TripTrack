import { Routes } from "@angular/router";
import { ProfileComponent } from "./profile/profile.component";
import { VerifyAccountComponent } from "./verified/verifyAccount.component"
import { HelpComponent } from "./help/help.component";
import { ForgotComponent } from "./forgot/forgot.component";
import { ResetComponent } from "./forgot/reset.component";


import { VERIFY_ROUTES } from "./verified/verifyAccount.routing";

// Prevent login access
import { AuthGuard } from "../_guards/auth.guard";

export const USER_ROUTES: Routes = [
    { path: '', redirectTo: 'profile', pathMatch: 'full' },
    { path: 'profile', canActivate: [AuthGuard], component: ProfileComponent },
    { path: 'verify', component: VerifyAccountComponent, children: VERIFY_ROUTES },
    { path: 'help', component: HelpComponent },
    { path: 'forgot', component: ForgotComponent },
    { path: 'forgot/reset/:token', component: ResetComponent }
];