import { Component, OnInit, OnDestroy, Input } from "@angular/core";
import { FormGroup, FormControl, Validators, NgForm } from "@angular/forms";

import { User } from "../../_models/user.model";
import { UserService } from "../../_services/user.service";
import { AuthService } from "../../_services/auth.service";
import { AlertService } from "../../_services/alert.service";
import { Email } from "../../_models/email.model";
import { Subscription } from "rxjs/Subscription";

@Component({
  selector: "app-profile",
  templateUrl: "./profile.component.html"
})
export class ProfileComponent implements OnInit, OnDestroy {
  profileForm: FormGroup;
  private userSub: Subscription;
  user: User;

  countries = [
    {id: 1, name: "United States"},
    {id: 2, name: "Belgium"},
    {id: 3, name: "Germany"},
    {id: 4, name: "The Netherlands"},
    {id: 5, name: "England"}
  ];

  constructor(
    private userService: UserService,
    private alertService: AlertService,
    private authSerice: AuthService
  ) {}

  onSubmit() {
    if (this.user) {
      // Edit
      var userForUpd = new User(
        this.user.provider,
        this.user.displayName,
        this.profileForm.value.familyName,
        this.profileForm.value.givenName,
        this.user.isVerified,
        this.user.dateCreated,
        this.user.emails,
        this.user.photos,
        this.profileForm.value.username,
        this.user.middleName,
        this.profileForm.value.description,
        this.profileForm.value.webUrl,
        this.profileForm.value.nationality,
        this.user.userId
      );
      this.user = userForUpd;
      this.userService.updateUser(this.user).subscribe(
        result => {
          this.alertService.success(result.message);
        },
        error => {
          this.alertService.error(error.message);
        }
      );
    }
  }

  ngOnInit() {
    this.findUser();
    this.profileForm = new FormGroup({
      givenName: new FormControl(null, [Validators.required]),
      familyName: new FormControl(null, [Validators.required]),
      username: new FormControl(null),
      nationality: new FormControl(null, [Validators.required]),
      webUrl: new FormControl(null, [Validators.required]),
      description: new FormControl(null, [Validators.required]),
      email: new FormControl({ value: "", disabled: true }, [
        Validators.required,
        Validators.pattern("")
      ]),
      password: new FormControl(null, [Validators.required])
    });
  }

  findUser() {
    this.userSub = this.authSerice.getUserLoggedIn().subscribe(userLoggedIn => {
      this.user = userLoggedIn;
    });
  }

  ngOnDestroy(): void {
    this.userSub.unsubscribe();
  }
}
