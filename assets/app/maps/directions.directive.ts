
import { GoogleMapsAPIWrapper } from '@agm/core';
import { Directive, Input, Output, OnInit } from '@angular/core';
import { Stopover } from '../_models/stopover.model';
import { validateConfig } from '@angular/router/src/config';
declare var google: any;



@Directive({
    selector: 'directions'
})
export class DirectionsMapDirective implements OnInit{
    ngOnInit(): void {
        this.gmapsApi.getNativeMap().then(map => {
            this.map = map; 
            this.infoWindow = new google.maps.InfoWindow;
            this.service = new google.maps.places.PlacesService(map);
            this.directionsService = new google.maps.DirectionsService;
            this.directionsDisplay = new google.maps.DirectionsRenderer;
        });
    }

    @Input() origin: any;
    @Input() destination: any;
    @Input() originPlaceId: any;
    @Input() destinationPlaceId: any;
    @Input() waypoints: any = [];
    @Input() directionsDisplay: any;
    @Input() estimatedTime: any;
    @Input() estimatedDistance: any;
    @Input() stopovers: any = [];
    @Input() sourceStopover: Stopover;
    @Input() destinationStopover: Stopover;
    @Input() transportMode: any;

    public map : any;
    public infoWindow : any;
    public service : any;
    public directionsService : any; 

    constructor(private gmapsApi: GoogleMapsAPIWrapper) {}
    reset() {
        this.directionsDisplay.setMap(null);
        //this.directionsDisplay.setDirections(null);

        this.origin = undefined;
        this.destination = undefined;
        this.waypoints = [];
        this.stopovers = [];
        this.sourceStopover = undefined;
        this.destinationStopover = undefined;
        this.transportMode = undefined;
        //this.directionsDisplay = undefined;
    }
    updateDirections() {
         
            var me = this;
            var latLngA = new google.maps.LatLng({ lat: this.origin.latitude, lng: this.origin.longitude });
            var latLngB = new google.maps.LatLng({ lat: this.destination.latitude, lng: this.destination.longitude });
            this.directionsDisplay.setMap(this.map);
            this.directionsDisplay.setPanel(document.getElementById('directionsList'));
            
            this.directionsDisplay.setOptions({
                polylineOptions: {
                    strokeWeight: 8,
                    strokeOpacity: 0.7,
                    strokeColor: '#00468c'
                }
            });
            this.directionsDisplay.setDirections({ routes: [] });
            var mode = google.maps.DirectionsTravelMode.DRIVING;
            switch (this.transportMode) {
                case "driving":
                    mode = google.maps.DirectionsTravelMode.DRIVING;
                    break;
                case "Walking":
                    mode = google.maps.DirectionsTravelMode.WALKING;
                    break;
                case "bicycling":
                    mode = google.maps.DirectionsTravelMode.BICYCLING;
                    break;
                case "transit":
                    mode = google.maps.DirectionsTravelMode.TRANSIT;
                    break;
                default:
                    break;
            }
            this.directionsService.route({
                origin: new google.maps.LatLng({ lat: this.origin.latitude, lng: this.origin.longitude }),
                destination: new google.maps.LatLng({ lat: this.destination.latitude, lng: this.destination.longitude }),
                waypoints: this.waypoints,
                optimizeWaypoints: true,
                travelMode: mode
            }, function (response: any, status: any) {
                // console.log(response);
                if (status === 'OK') {
                    me.directionsDisplay.setDirections(response);
                    var point = response.routes[0].legs[0];
                    me.estimatedTime = point.duration.text;
                    me.estimatedDistance = point.distance.text;
                    // console.log(me.estimatedTime);
                    // console.log('Estimated travel time: ' + point.duration.text + ' (' + point.distance.text + ')');

                } else {
                    // console.log('Directions request failed due to ' + status);
                }
            });

    }

    private getcomputeDistance(latLngA: any, latLngB: any) {
        return (google.maps.geometry.spherical.computeDistanceBetween(latLngA, latLngB) / 1000).toFixed(2);
    }
}