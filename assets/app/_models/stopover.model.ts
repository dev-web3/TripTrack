import { User } from "./user.model";
import { Travel } from "./travel.model";

export class Stopover {

    constructor(public city: string, 
                public position: number, 
                public user: User, 
               // public travel: Travel,
                public latitude: number,
                public longitude: number){}
}