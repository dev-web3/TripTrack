import { Email } from "./email.model";
import { Photo } from "./photo.model";
export class User {
  provider: string;
  displayName: string;
  familyName: string;
  givenName: string;
  isVerified: boolean;
  dateCreated: Date;
  emails: Email[];
  photos: Photo[];

  username?: string;
  middleName?: string;
  description?: string;
  webUrl?: string;
  nationality?: string;
  userId?: string;

  constructor(
    provider: string,
    displayName: string,
    familyName: string,
    givenName: string,
    isVerified: boolean,
    dateCreated: Date,
    emails: Email[],
    photos: Photo[],
    username?: string,
    middleName?: string,
    description?: string,
    webUrl?: string,
    nationality?: string,
    userId?: string
  ) {
    this.provider = provider;
    this.displayName = displayName;
    this.familyName = familyName;
    this.givenName = givenName;
    this.isVerified = isVerified;
    this.dateCreated = dateCreated;
    this.emails = emails;
    this.photos = photos;

    this.username = username;
    this.middleName = middleName;
    this.description = description;
    this.webUrl = webUrl;
    this.nationality = nationality;
    this.userId = userId;
  }
}
