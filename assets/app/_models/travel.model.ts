import { User } from "../_models/user.model";
import { Stopover } from "./stopover.model";
    
export class Travel {

    constructor(public travelName: string, 
                public startDate:Date,
                public travelMode:string,
                public users: User[], 
                public stopovers: Stopover[]){}

                showDetails: boolean;
                
}