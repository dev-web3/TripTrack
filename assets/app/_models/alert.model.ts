export class Alert{

    constructor(public type: AlertType, public message: string, public state = 'active'){}

}

export enum AlertType{
    Success,
    Error,
    Info,
    Warning
}