import { Component, OnInit, ViewChild, ElementRef, NgZone } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { MapsAPILoader, GoogleMapsAPIWrapper } from "@agm/core";
import { Marker } from "../_models/marker.model";
import { TravelService } from "../_services/travel.service";
import { Travel } from "../_models/travel.model";
import { Stopover } from "../_models/stopover.model";
import { User } from "../_models/user.model";
import { forEach } from "@angular/router/src/utils/collection";
import { FormArray } from "@angular/forms/src/model";
import { DirectionsMapDirective } from "../maps/directions.directive";
import { Input } from "@angular/core";
import { AlertService } from "../_services/alert.service";

@Component({
    selector: 'new-travel',
    styles: [`
    agm-map {
      height: 500px;
    }
  `],
    templateUrl: './new-travel.component.html',
    providers: [GoogleMapsAPIWrapper]
})
export class NewTavelComponent implements OnInit {

    latitude: number;
    longitude: number;
    searchForm: FormGroup;
    zoom: number;
    markers: Marker[] = [];
    //stopovers: Stopover[] = [];
    autocompleteSource;
    autocompleteDestination;
    autocompleteStopover;
    @Input()
    directionComponent: DirectionsMapDirective;
    @Input()
    travels: Travel[];

    @ViewChild("source")
    public sourceElementRef: ElementRef;

    @ViewChild("destination")
    public destinationElementRef: ElementRef;

    @ViewChild("stopover")
    public stopoverElementRef: ElementRef;

    showmapBoolean : Boolean = false;
    constructor(
        private mapsAPILoader: MapsAPILoader,
        private ngZone: NgZone,
        private mapWrapper: GoogleMapsAPIWrapper,
        public travelService: TravelService,
        private alertService: AlertService
    ) {
    }

    onSave() {

        if (this.directionComponent.sourceStopover == null || this.directionComponent.destinationStopover == null) {
            // console.log("Aucuns stopovers");
            return;

        }
        //this.searchForm.get('travelMode').value;
        //this.searchForm.get('startDate').value;

        var user: User;
        const users: User[] = [];
        users.push(user)
        const stopovers = [];
        //console.log(this.searchForm.get('startDate').value);
        //console.log(this.searchForm.get('startDate').value);
        
        const travel = new Travel(this.searchForm.get('travelName').value,new Date(this.searchForm.get('startDate').value),this.searchForm.get('travelMode').value, users, stopovers);

        var position = 0;
        this.directionComponent.sourceStopover.position = position;
        travel.stopovers.push(this.directionComponent.sourceStopover);
        position++;
        for (let waypoint of this.directionComponent.stopovers) {
            waypoint.position = position;
            position++;
            travel.stopovers.push(waypoint);
        }
        this.directionComponent.destinationStopover.position = position;
        travel.stopovers.push(this.directionComponent.destinationStopover);



        this.travelService.save(travel).subscribe(
            data => {
                this.alertService.success(data.message);
            },
            error => {
                this.alertService.error(error.message);
            }
        );

        this.directionComponent.updateDirections();
        
        this.travelService.getTravels()
            .subscribe(
            (travels: Travel[]) => {
                this.travels = travels;
                console.log("getTravels");
            }
        );

    }
    onReset() {
        this.searchForm.reset();
        this.searchForm.patchValue({ travelMode: 'driving' });
        this.setCurrentPosition();
        this.directionComponent.reset();
        this.showmapBoolean = false; 
    }
    showMap() {
        // console.log("Source : "+this.searchForm.get('source').value);
        // console.log("Destination : "+this.searchForm.get('destination').value);
        //get the place result
        this.ngZone.run(() => {
            let placeSource: google.maps.places.PlaceResult = this.autocompleteSource.getPlace();
            let placeDestination: google.maps.places.PlaceResult = this.autocompleteDestination.getPlace();
            //verify result
            if (placeSource.geometry === undefined || placeSource.geometry === null) {
                return;
            }
            if (placeDestination.geometry === undefined || placeDestination.geometry === null) {
                return;
            }
            //set latitude, longitude and zoom
            var latSource = placeSource.geometry.location.lat();
            var longSource = placeSource.geometry.location.lng();
            var latDestination = placeDestination.geometry.location.lat();
            var longDestination = placeDestination.geometry.location.lng();
            this.markers = [];

            this.longitude = longSource;
            this.latitude = latSource;
            this.directionComponent.origin = { longitude: longSource, latitude: latSource };
            this.directionComponent.originPlaceId = placeSource.place_id;
            this.directionComponent.destination = { longitude: longDestination, latitude: latDestination }; // its a example aleatory position
            this.directionComponent.destinationPlaceId = placeDestination.place_id;

            this.directionComponent.transportMode = this.searchForm.get('travelMode').value;
            var user: User;

            this.directionComponent.sourceStopover = new Stopover(placeSource.name, 5, user, latSource, longSource);
            this.directionComponent.destinationStopover = new Stopover(placeDestination.name, 5, user, latDestination, longDestination);

            if (this.directionComponent.directionsDisplay === undefined) {
                this.mapsAPILoader.load().then(() => {
                    this.directionComponent.directionsDisplay = new google.maps.DirectionsRenderer;
                });
            }

            this.directionComponent.updateDirections();
            this.zoom = 12;
            this.showmapBoolean = true;
        })
    }


    ngOnInit(): void {

        //set google maps defaults
        this.zoom = 4;
        this.latitude = 39.8282;
        this.longitude = -98.5795;
        // console.log(this.directionComponent);
        //create search FormControl
        this.searchForm = new FormGroup({
            travelName: new FormControl('', Validators.required),
            startDate: new FormControl('', Validators.required),
            source: new FormControl('', Validators.required),
            destination: new FormControl('', Validators.required),
            travelMode: new FormControl('', Validators.required)
        });
        this.searchForm.patchValue({ travelMode: 'driving' });

        //set current position
        this.setCurrentPosition();
        this.mapsAPILoader.load().then(() => {
            this.autocompleteSource = new google.maps.places.Autocomplete(this.sourceElementRef.nativeElement);
            this.autocompleteDestination = new google.maps.places.Autocomplete(this.destinationElementRef.nativeElement);
            this.autocompleteStopover = new google.maps.places.Autocomplete(this.stopoverElementRef.nativeElement);
        });
    }
    onAddStopover(stopoverForm: FormGroup) {
        if (this.directionComponent.sourceStopover === undefined || this.directionComponent.destinationStopover === undefined) {
            return;
        }
        let placeStopOver: google.maps.places.PlaceResult = this.autocompleteStopover.getPlace();
        //verify result
        if (placeStopOver.geometry === undefined || placeStopOver.geometry === null) {
            return;
        }
        var lat = placeStopOver.geometry.location.lat();
        var long = placeStopOver.geometry.location.lng();

        this.directionComponent.waypoints.push({
            location: placeStopOver.formatted_address,
            stopover: true,
        });
        var user: User;
        this.directionComponent.stopovers.push(new Stopover(placeStopOver.name, 5, user, lat, long));

        // console.log(placeStopOver.formatted_address);
        if (this.directionComponent.directionsDisplay === undefined) {
            this.mapsAPILoader.load().then(() => {
                this.directionComponent.directionsDisplay = new google.maps.DirectionsRenderer;
            });
        }
        this.directionComponent.updateDirections();
        this.zoom = 12;

        //this.markers.push(new Marker(long, lat));
        //this.stopovers.push(new Stopover(placeStopOver.adr_address, this.stopovers.length + 1))
    }
    private setCurrentPosition() {
        if ("geolocation" in navigator) {
            navigator.geolocation.getCurrentPosition((position) => {
                this.latitude = position.coords.latitude;
                this.longitude = position.coords.longitude;
                this.zoom = 12;
            });
        }
    }


}