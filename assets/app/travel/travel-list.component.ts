import { Component, OnInit, NgZone, ViewChild } from "@angular/core";
import { Travel } from "../_models/travel.model";
import { TravelService } from "../_services/travel.service";
import { MapsAPILoader, GoogleMapsAPIWrapper } from "@agm/core";
import { DirectionsMapDirective } from "../maps/directions.directive";
import { Stopover } from "../_models/stopover.model";
import { forEach } from "@angular/router/src/utils/collection";
import { StopoverListComponent } from "../stopover/stopover-list.component";
import { Subject } from "rxjs";
import { AlertService } from "../_services/alert.service";

@Component({
  selector: "app-travel-list",
  styles: [
    `
    agm-map {
      height: 100%;
    }
    `
  ],
  templateUrl: "./travel-list.component.html",
  providers: [GoogleMapsAPIWrapper]
})
export class TravelListComponent implements OnInit {
  travels: Travel[];
  newTravel: boolean = false;
  showList: boolean = false;
  stopovers: Stopover[];
  nbStopovers: number = 0;

  latitude: number;
  longitude: number;
  zoom: number;

  tabMarkers: any = [];

  travelClicked: Subject<Travel> = new Subject();

  @ViewChild(DirectionsMapDirective) directionComponent: DirectionsMapDirective;

  constructor(
    public travelService: TravelService,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    private mapWrapper: GoogleMapsAPIWrapper,
    private alertService: AlertService
  ) {}

  ngOnInit() {
    this.tabMarkers = [];

    this.travelService.getTravels().subscribe((travels: Travel[]) => {
      this.travels = travels;
    }, error => this.alertService.error(error.message));
    //set google maps defaults
    this.zoom = 4;
    this.latitude = 48.856614;
    this.longitude = 2.3522219000000177;

    //set current position
    this.setCurrentPosition();

    this.mapsAPILoader.load();
  }

  showTravelDetails(travel: Travel) {
    //afficher la liste de stopovers
    this.travelClicked.next(travel);

    //afficher les markers et chemins
    this.showDetails(travel);
    travel.showDetails = true;
  }

  createNewTrip() {
    this.newTravel = true;

    this.clearMarkers();

    this.directionComponent.reset();
  }

  showDetails(travel: Travel) {
    this.clearMarkers();

    this.newTravel = false;

    this.directionComponent.stopovers = [];
    this.directionComponent.waypoints = [];

    this.stopovers = travel.stopovers;
    this.nbStopovers = 0;

    this.stopovers.forEach(element => {
      if (this.nbStopovers == 0) {
        this.directionComponent.origin = {
          latitude: element.latitude,
          longitude: element.longitude
        };
      } else if (this.nbStopovers == this.stopovers.length - 1) {
        this.directionComponent.destination = {
          latitude: element.latitude,
          longitude: element.longitude
        };
        if (this.directionComponent.directionsDisplay === undefined) {
          this.mapsAPILoader.load().then(() => {
            this.directionComponent.directionsDisplay = new google.maps.DirectionsRenderer();
            this.directionComponent.directionsDisplay.setPanel(
              document.getElementById("directionsList")
            );
            this.showList = true;
          });
        }
        this.directionComponent.updateDirections();
        this.zoom = 12;
      } else {
        this.directionComponent.waypoints.push({
          location: new google.maps.LatLng(element.latitude, element.longitude),
          stopover: true
        });
      }
      this.nbStopovers++;
    });
    this.showList = true;

    ///////////////////////////////////////////////////////////////////////////////////////
    // This example requires the Places library. Include the libraries=places
    // parameter when you first load the API. For example:
    // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

    var map;
    var infoWindow;
    var service;
    var tab = this.tabMarkers;

    map = this.directionComponent.map;
    infoWindow = this.directionComponent.infoWindow;
    service = this.directionComponent.service;

    for (let stopover of this.stopovers) {
      var request = {
        location: new google.maps.LatLng(stopover.latitude, stopover.longitude),
        radius: 20000,
        keyword: "best view"
      };
      if (service === undefined) {
        service = new google.maps.places.PlacesService(map);
      }
      service.radarSearch(request, callback);
    }

    function callback(results, status) {
      if (status !== google.maps.places.PlacesServiceStatus.OK) {
        return;
      }
      for (var i = 0, result; (result = results[i]); i++) {
        addMarker(result);
      }
    }

    function addMarker(place) {
      var marker = new google.maps.Marker({
        map: map,
        position: place.geometry.location,
        icon: {
          url:
            "https://developers.google.com/maps/documentation/javascript/images/circle.png",
          anchor: new google.maps.Point(10, 10),
          scaledSize: new google.maps.Size(10, 17)
        }
      });

      tab.push(marker);

      google.maps.event.addListener(marker, "click", function() {
        service.getDetails(place, function(result, status) {
          if (status !== google.maps.places.PlacesServiceStatus.OK) {
            return;
          }

          var image;
          if (result.photos === undefined) image = "<br> No Picture <br><br>";
          else
            image =
              '<img *ngIf="show" src="' +
              result.photos[0].getUrl({ maxWidth: 100, maxHeight: 100 }) +
              '" width="83" height="115">';

          var content =
            '<div id="iw_container">' +
            '<div class="iw_title">' +
            result.name +
            "</div>" +
            image +
            '<div class="iw-subTitle" ><u>Contacts</u></div>' +
            "<p>Address : " +
            result.formatted_address +
            "<br>Phone number : " +
            result.formatted_phone_number +
            "</p>" +
            '<a href="' +
            result.website +
            '" target="_blank" >site web</a>';
          "</div>";
          //infoWindow.setContent(result.name + "\n" + result.website);
          infoWindow.setContent(content);
          infoWindow.open(map, marker);
        });
      });
    }
  }

  private clearMarkers() {
    for (var i = 0; i < this.tabMarkers.length; i++) {
      this.tabMarkers[i].setMap(null);
    }
    this.tabMarkers.length = 0;
  }

  private setCurrentPosition() {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition(position => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 12;
      });
    }
  }
}
