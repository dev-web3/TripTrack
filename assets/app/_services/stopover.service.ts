import { Stopover } from "../_models/stopover.model";
import { Http, Response} from "@angular/http";
import { Injectable } from "@angular/core";
import 'rxjs/Rx';
import { Observable } from "rxjs";

@Injectable()
export class StopoverService {

    stopovers: Stopover[] = [];

    constructor(private http: Http){}

    getStopovers(){
        return this.http.get('/stopover')
        .map((response: Response) => {
            const stopovers = response.json().obj;
            let transformedStopovers: Stopover[] = [];
            for (let stopover of stopovers){
                transformedStopovers.push(new Stopover(stopover.city, stopover.position, stopover.user, stopover.latitude, stopover.longitude));
                console.log(stopover);
            }
            this.stopovers = transformedStopovers;
            return transformedStopovers;
        })
        .catch((error: Response) => Observable.throw(error));
    }


}