import { Injectable } from "@angular/core";

import {JwtHelper} from '../_helpers/jwt.helper';
@Injectable()
export class TokenService {

    constructor(private jwtHelper: JwtHelper){}

    decodeToken(token: string){
        return this.jwtHelper.decodeToken(token);
    }


}