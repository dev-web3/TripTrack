import { Injectable } from "@angular/core";
// import * as io from "socket.io-client";
import { Socket } from "ng-socket-io";
import { Subject } from "rxjs";
import { Observable } from "rxjs/Observable";

@Injectable()
export class SocketService {
  constructor(public socket: Socket) {}

  getNbUsersOnline() {
    let observable = new Observable(observer => {
      this.socket.on("user count", function(nbUsers) {
        observer.next(nbUsers);
      });
    });
    return observable;
  }
}
