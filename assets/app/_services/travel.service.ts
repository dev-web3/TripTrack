import { Travel } from "../_models/travel.model";
import { Http, Response, Headers} from "@angular/http";
import { Injectable } from "@angular/core";
import 'rxjs/Rx';
import { Observable } from "rxjs";


@Injectable()
export class TravelService {
    travels: Travel[] = [];

    constructor(private http: Http){}
    
    save(travel:Travel){

        const body = JSON.stringify(travel);
        console.log(body);
        const headers = new Headers({'Content-Type':'application/json'});
        return this.http.post('/maps',body,{headers:headers})
            .map((response :Response)=>response.json())
            .catch((error: Response)=>Observable.throw(error));
    }
 
    getTravels(){
        return this.http.get('/back_travels')
        .map((response: Response) => {
            const travels = response.json().obj;
            let transformedTravels: Travel[] = [];
            for (let travel of travels){
                transformedTravels.push(new Travel(travel.travelName, travel.startDate, travel.travelMode ,travel.users, travel.stopovers));
            }
            this.travels = transformedTravels;
            return transformedTravels;
        })
        .catch((error: Response) => Observable.throw(error));
    }
}