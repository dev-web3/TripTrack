import { Http, Response, Headers } from "@angular/http";
import { Injectable, EventEmitter } from "@angular/core";
import "rxjs/Rx";
import { Observable } from "rxjs";
import { Router } from "@angular/router";

import { User } from "../_models/user.model";
import { Email } from "../_models/email.model";
import { Photo } from "../_models/photo.model";

import { TokenService } from "./token.service";
import { AuthService } from "./auth.service";
import { UserHelper } from "../_helpers/user.helper";

@Injectable()
export class UserService {
  constructor(
    private http: Http,
    private tokenService: TokenService,
    private router: Router,
    private authService: AuthService,
    private userHelper: UserHelper
  ) {}

  updateUser(user: User) {
    const body = JSON.stringify(user);
    const headers = new Headers({ "Content-Type": "application/json" });
    return this.http
      .patch("/account/update", body, {
        headers: headers
      })
      .map((response: Response) => {
        var parsedRes = response.json();
        var userUpdated = this.userHelper.parseUser(parsedRes.obj);
        this.authService.UserLoggedIn = userUpdated;
        return parsedRes;
      })
      .catch((error: Response) => Observable.throw(error.json()));
  }

  verifyUser(verifyToken: string) {
    return this.http
      .get("/account/verify/" + verifyToken)
      .map((response: Response) => response.json())
      .catch((error: Response) => Observable.throw(error.json()));
  }

  resendVerifyAccountEmail(email: string) {
    var json = {
      email: email
    };
    const body = JSON.stringify(json);
    const headers = new Headers({ "Content-Type": "application/json" });
    return this.http
      .post("/account/resend", body, {
        headers: headers
      })
      .map((response: Response) => response.json())
      .catch((error: Response) => Observable.throw(error.json()));
  }

  forgot(email: string) {
    var json = {
      email: email
    };
    const body = JSON.stringify(json);
    const headers = new Headers({ "Content-Type": "application/json" });
    return this.http
      .post("/account/forgot", body, { headers: headers })
      .map((response: Response) => response.json())
      .catch((error: Response) => Observable.throw(error.json()));
  }

  reset(password: string, token: string){
    var json = {
      password: password
    }
    const body = JSON.stringify(json);
    const headers = new Headers({ "Content-Type": "application/json" });
    return this.http
      .post("/account/reset/" + token , body, { headers: headers })
      .map((response: Response) => response.json())
      .catch((error: Response) => Observable.throw(error.json()));
  }
}
