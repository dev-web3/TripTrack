import { Injectable } from "@angular/core";
import { Http, Headers, Response } from "@angular/http";
import "rxjs/Rx";
import { Observable } from "rxjs";

import { User } from "../_models/user.model";
import { Credentials } from "../_models/credentials.model";
import { CanActivate } from "@angular/router/src/interfaces";
import { ActivatedRouteSnapshot, Router } from "@angular/router";

import { TokenService } from "./token.service";
import { UserService } from "./user.service";

import { UserHelper } from "../_helpers/user.helper";

@Injectable()
export class AuthService {
  private userLoggedIn: User;
  private isLoggedIn: boolean = false;
  private redirectUrl: string;

  constructor(
    private http: Http,
    private tokenService: TokenService,
    private router: Router,
    private userHelper: UserHelper
  ) {}

  set UserLoggedIn(user: User) {
    this.userLoggedIn = user;
  }

  get UserLoggedIn() {
    return this.userLoggedIn;
  }

  set IsLoggedIn(isLoggedIn: boolean) {
    this.isLoggedIn = isLoggedIn;
  }

  get IsLoggedIn(): boolean {
    return this.isLoggedIn;
  }

  set RedirectUrl(url: string) {
    this.redirectUrl = url;
  }

  get RedirectUrl() {
    return this.redirectUrl;
  }

  getUserLoggedIn(){
    let observable = new Observable<User>(observer => {
      observer.next(this.userLoggedIn);
    });
    return observable;
  }

  signup(user: Credentials) {
    const body = JSON.stringify(user);
    const headers = new Headers({ "Content-Type": "application/json" });
    return this.http
      .post("/auth/signup", body, { headers: headers })
      .map((response: Response) => response.json())
      .catch((error: Response) => {
        return Observable.throw(error.json());
      });
  }

  signin(user: Credentials) {
    const body = JSON.stringify(user);
    const headers = new Headers({ "Content-Type": "application/json" });
    return this.http
      .post("/auth/signin", body, { headers: headers })
      .map((response: Response) => {
        return response.json();
      })
      .catch((error: Response) => {
        // traitement d'affichage erreur
        return Observable.throw(error.json());
      });
  }

  logout() {
    this.isLoggedIn = false;
    this.UserLoggedIn = null;
    return this.http
      .get("/auth/logout", { withCredentials: true })
      .map((response: Response) => response.json())
      .catch((error: Response) => {
        // traitement d'affichage erreur
        return Observable.throw(error.json());
      });
  }

  init() {
    const headers = new Headers({ "Content-Type": "application/json" })
    return this.http
      .get("/auth/isAuthenticated")
      .map((response: Response) => {
        try{
            var parsedResponse = response.json();
            var parsedToken = this.tokenService.decodeToken(parsedResponse.obj);
            var printedUser = this.userHelper.parseUser(parsedToken.user);
            localStorage.setItem("user", parsedResponse.obj);
            this.UserLoggedIn = printedUser;
            this.IsLoggedIn=true;
            let redirectUrl = this.RedirectUrl ? this.RedirectUrl : '/travels';
            this.router.navigate([redirectUrl]);
            return response.json();
        }catch(err){
            this.router.navigateByUrl("/");
        }

      })
      .catch((error: Response) => {
        this.userLoggedIn = null;
        this.isLoggedIn = false;
        this.router.navigateByUrl("/");
        return Observable.throw(error.json());
      });
  }
}
