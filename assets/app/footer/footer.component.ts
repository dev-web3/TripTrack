import { Component, OnInit, Input } from "@angular/core";
import { SocketService } from "../_services/socket.service";
import { Observable } from "rxjs/Observable";
import { OnDestroy } from "@angular/core/src/metadata/lifecycle_hooks";
import { ISubscription } from "rxjs/Subscription";
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: "app-footer",
  templateUrl: "./footer.component.html"
})
export class FooterComponent implements OnDestroy {
  @Input() nbUserConnected: {} = 0;
  _userCounterSubscription: ISubscription;
  translate: TranslateService;

  constructor(private socketService: SocketService, translate: TranslateService) {
    this.translate = translate;
  }

  ngOnInit(): void {
    this._userCounterSubscription = this.socketService
      .getNbUsersOnline()
      .subscribe(nbUser => {
        this.nbUserConnected = nbUser;
      });
  }

  switchLanguage(lang){
    this.translate.use(lang);
  }

  getCurrentLanguage(){
    return this.translate.currentLang;
  }

  ngOnDestroy(): void {
    this._userCounterSubscription.unsubscribe();
  }
}
