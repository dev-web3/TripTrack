import { Component, Input, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { AuthService } from "../_services/auth.service";

import { User } from "../_models/user.model";
import { OnDestroy } from "@angular/core/src/metadata/lifecycle_hooks";
import { subscriptionLogsToBeFn } from "rxjs/testing/TestScheduler";
import { Subscription } from "rxjs";


@Component({
    selector: 'app-header',
    templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit, OnDestroy{

    private userSub: Subscription;

    @Input() user:User;
    @Input() profilePhotoUrl: String;

    constructor(private authService: AuthService){}

    ngOnInit(): void {
        if(this.isConnected()){
            this.userSub = this.authService.getUserLoggedIn().subscribe(userLoggedIn => {
                this.user = userLoggedIn;
            });
            this.profilePhotoUrl = this.user.photos[0]? this.authService.UserLoggedIn.photos[0].value : "/img/user-circle-o.svg";
        }
    }

    isConnected(){
        return this.authService.IsLoggedIn;
    }

    ngOnDestroy(): void {
        if(this.userSub){
            this.userSub.unsubscribe();
        }
    }

}