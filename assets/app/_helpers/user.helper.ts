import { User } from "../_models/user.model";
import { Email } from "../_models/email.model";
import { Photo } from "../_models/photo.model";
export class UserHelper{

    parseUser(user: any){
        let transformedEmails: Email[] = [];
        for (let email of user.emails) {
          transformedEmails.push(new Email(email.value, email.type));
        }
        let transformedPhotos: Photo[] = [];
        for (let photo of user.photos) {
          transformedPhotos.push(new Photo(photo.value));
        }
    
        var printedUser = new User(
          user.provider,
          user.displayName,
          user.name.familyName,
          user.name.givenName,
          user.isVerified,
          new Date(user.date_created),
          transformedEmails,
          transformedPhotos,
          user.username,
          user.name.middleName,
          user.description,
          user.webUrl,
          user.nationality,
          user._id
        );
        return printedUser;
      }
}