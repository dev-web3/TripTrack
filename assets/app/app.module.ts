import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from "@angular/core";
import { SocketIoModule, SocketIoConfig } from "ng-socket-io";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpModule } from '@angular/http';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';


import { AppComponent } from "./app.component";
import { HeaderComponent } from "./header/header.component";
import { FooterComponent } from "./footer/footer.component";
import { routing } from "./app.routing";
import { AlertComponent } from "./alert/alert.component";
import {HttpClientModule, HttpClient} from '@angular/common/http';

const config: SocketIoConfig = { url: "/", options: {} };


// HELPERS
import { JwtHelper } from "./_helpers/jwt.helper";
import { UserHelper } from "./_helpers/user.helper";

// GUARDS
import { AuthGuard } from "./_guards/auth.guard";

// MODELS

// SERVICES
import { UserService } from "./_services/user.service";
import { AuthService } from "./_services/auth.service";
import { TokenService } from "./_services/token.service";
import { SocketService } from "./_services/socket.service";
import { TravelService } from "./_services/travel.service";
import { AlertService } from "./_services/alert.service";

// Authentification
import { AuthenticationComponent } from "./auth/authentication.component";
import { SigninComponent } from "./auth/signin/signin.component";
import { SignupComponent } from "./auth/signup/signup.component";
import { LogoutComponent } from "./auth/logout/logout.component";
import { SuccessLoginComponent } from "./auth/successLogin/success-login.component";

// User
import { UserComponent } from "./user/user.component";
import { ProfileComponent } from "./user/profile/profile.component";
import { VerifyAccountComponent } from "./user/verified/verifyAccount.component";
import { ResendVerificationMailComponent } from "./user/verified/account/resendVerificationMail.component";
import { ConfirmAccountComponent } from "./user/verified/account/confirmAccount.component";
import { HelpComponent } from "./user/help/help.component";
import { ForgotComponent } from "./user/forgot/forgot.component";
import { ResetComponent } from "./user/forgot/reset.component";

import { TravelsComponent } from "./travel/travels.component";
import { TravelListComponent } from "./travel/travel-list.component";

import { StopoverComponent } from "./stopover/stopover.component";
import { StopoverListComponent } from "./stopover/stopover-list.component";
import { AgmCoreModule, GoogleMapsAPIWrapper } from "@agm/core";
import { DirectionsMapDirective } from "./maps/directions.directive";
import { Http } from '@angular/http';

import { UnauthorizedComponent } from "./unauthorized/unauthorized.component";
import { PageNotFoundComponent } from "./pageNotFound/not-found.component";
import { NewTavelComponent } from "./travel/new-travel.component";

export function HttpLoaderFactory(http: Http) {
    return new TranslateHttpLoader(http, '/assets/i18n/', '.json');
}
  

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    AlertComponent,

    // User
    ProfileComponent,
    VerifyAccountComponent,
    ResendVerificationMailComponent,
    ConfirmAccountComponent,
    HelpComponent,
    ForgotComponent,
    ResetComponent,


    // Authentification
    AuthenticationComponent,
    UserComponent,
    LogoutComponent,
    SignupComponent,
    SigninComponent,
    SuccessLoginComponent,

    // MAPS
    DirectionsMapDirective,

    //NEWS
    TravelsComponent,
    TravelListComponent,
    NewTavelComponent,

    StopoverComponent,
    StopoverListComponent,

    // Others
    UnauthorizedComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    SocketIoModule.forRoot(config),
    FormsModule,
    routing,
    ReactiveFormsModule,
    HttpModule,
    HttpClientModule,
    TranslateModule.forRoot({
        loader: {
            provide: TranslateLoader,
            useFactory: (HttpLoaderFactory),
            deps: [Http]
        }
    }),
    AgmCoreModule.forRoot({
      apiKey: "AIzaSyDIR53hsY5cIA1-xzxlnf7LntT4bu0XWIk",
      libraries: ["places"]
    })
  ],
  providers: [
    UserService,
    TokenService,
    AuthGuard,
    AuthService,
    SocketService,
    TravelService,
    AlertService,
    JwtHelper,
    UserHelper
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
