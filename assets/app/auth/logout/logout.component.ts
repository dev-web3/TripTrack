import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { AuthService } from "../../_services/auth.service";

@Component({
    selector: 'app-logout',
    templateUrl: './logout.component.html'
})
export class LogoutComponent implements OnInit{

    constructor(private authService: AuthService, private router: Router){}

    onLogout(){
        //console.log('logout.component - onLogout()');
        this.authService.logout()
            .subscribe();
        this.router.navigate(['/auth', 'signin']);
    }

    ngOnInit(): void {
        //console.log('logout.component - onInit()');
        this.authService.logout()
            .subscribe();
        localStorage.clear();
        this.router.navigateByUrl("/");
    }
}