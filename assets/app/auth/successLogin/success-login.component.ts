import { Component, OnInit} from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";

import { AuthService } from "../../_services/auth.service";
import { UserService } from "../../_services/user.service";
import { TokenService } from "../../_services/token.service";

import { User } from "../../_models/user.model";
import { Email } from "../../_models/email.model";

import { UserHelper } from "../../_helpers/user.helper";

@Component({
    selector:'app-authentication-success',
    template: `<p>Login success ! Rediricting to TripTrack !</p>`
})
export class SuccessLoginComponent implements OnInit{

    private token: string;

    constructor(private route: ActivatedRoute, private authService: AuthService, private tokenService: TokenService, 
        private router: Router, private userHelper: UserHelper) { }

    ngOnInit(): void {
        this.route.params.subscribe(params => {
            // console.log(params.token); // Print the parameter to the console.
            if(params['token']){
                this.token = params.token;
            }else{
                this.router.navigateByUrl("/unauthorized");
            }
        });
        try{
            var parsedToken = this.tokenService.decodeToken(this.token);
            var printedUser = this.userHelper.parseUser(parsedToken.user);
            localStorage.setItem("user", this.token);
            this.authService.UserLoggedIn = printedUser;
            this.authService.IsLoggedIn=true;
            let redirectUrl = this.authService.RedirectUrl ? this.authService.RedirectUrl : '/travels';
            //console.log("redirection: " + redirectUrl)
            this.router.navigate([redirectUrl]);
        }catch(err){
            //console.log("not good jwt");
            this.router.navigateByUrl("/");
        }
    }

}