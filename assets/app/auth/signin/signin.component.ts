import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import { ActivatedRoute } from "@angular/router";

import { AuthService } from "../../_services/auth.service";
import { AlertService } from "../../_services/alert.service";
import { Credentials } from "../../_models/credentials.model";

@Component({
  selector: "app-signin",
  templateUrl: "./signin.component.html"
})
export class SigninComponent implements OnInit {
  myForm: FormGroup;

  constructor(
    private translator: TranslateService,
    private authService: AuthService,
    private alertService: AlertService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  onSubmit() {
    if (this.myForm.valid) {
      var rememberMe;
      if (this.myForm.value.remember) {
        rememberMe = true;
      } else {
        rememberMe = false;
      }
      var credentials = new Credentials(
        this.myForm.value.email,
        this.myForm.value.password,
        null,
        null,
        rememberMe
      );
      this.authService.signin(credentials).subscribe(
        data => {
          this.router.navigateByUrl("/auth/success/" + data.jwt);
        },
        error => {
          this.alertService.error(error.message);
        }
      );
      this.initForm();
    }
  }

  ngOnInit() {
    this.initForm();
    localStorage.clear();
    this.authService.RedirectUrl = this.route.snapshot.queryParams["returnUrl"];
  }

  initForm() {
    this.myForm = new FormGroup({
      email: new FormControl(null, [
        Validators.required,
        Validators.pattern("")
      ]),
      password: new FormControl(null, Validators.required),
      remember: new FormControl({ checked: true }, [])
    });
  }
}
