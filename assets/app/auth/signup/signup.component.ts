import { Component, OnInit } from "@angular/core";
import { FormGroup, Validators, FormControl } from "@angular/forms";

import { AuthService } from "../../_services/auth.service";
import { AlertService } from "../../_services/alert.service";
import { Credentials } from "../../_models/credentials.model";
import { Router } from "@angular/router";

@Component({
  selector: "app-signup",
  templateUrl: "./signup.component.html"
})
export class SignupComponent implements OnInit {
  myForm: FormGroup;

  constructor(
    private authService: AuthService,
    private alertService: AlertService,
    private router: Router
  ) {}

  onSubmit() {
    if(this.myForm.valid){
      const credentials = new Credentials(
        this.myForm.value.email,
        this.myForm.value.password,
        this.myForm.value.firstName,
        this.myForm.value.lastName
      );
      this.authService.signup(credentials).subscribe(
        data => {
          this.alertService.success(data.message, true);
          this.router.navigateByUrl("/auth/signin");
          this.myForm.reset();
        },
        error => {
          this.alertService.error(error.message);
        }
      );
    }
  }

  ngOnInit() {
    this.myForm = new FormGroup({
      firstName: new FormControl(null, Validators.required),
      lastName: new FormControl(null, Validators.required),
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, Validators.required)
    });
  }
}
