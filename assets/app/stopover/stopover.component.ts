import { Component, Input } from "@angular/core";
import { Stopover } from "../_models/stopover.model";
import { TravelListComponent } from "../travel/travel-list.component";

@Component({
    selector: 'app-stopover',
    templateUrl: './stopover.component.html'
})

export class StopoverComponent {

    @Input() stopover : Stopover;

    constructor(private travelList : TravelListComponent){}

    public zoomOnStopoverPosition(){
        this.travelList.latitude = this.stopover.latitude;
        this.travelList.longitude = this.stopover.longitude;
        this.travelList.zoom = 13;
    }
    
}