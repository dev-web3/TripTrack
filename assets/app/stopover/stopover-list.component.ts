import { Component, Input, OnInit, OnDestroy } from "@angular/core";
import { Stopover } from "../_models/stopover.model";
import { Travel } from "../_models/travel.model";
import { Subject } from "rxjs";

@Component({
    selector: 'app-stopover-list',
    templateUrl: './stopover-list.component.html'
})

export class StopoverListComponent implements OnInit, OnDestroy {

    stopovers: Stopover[];

    @Input('travel')
    travel: Subject<Travel>;

    constructor(){}
    
    ngOnInit(){
        this.travel.subscribe(travel => {
            //console.log(travel);
            this.showStopoversList(travel);
        });
    }   

    showStopoversList(travel : Travel){
        this.stopovers = travel.stopovers;
        //console.log(this.stopovers);
    }

    ngOnDestroy(): void {
        this.travel.unsubscribe();
    }

}