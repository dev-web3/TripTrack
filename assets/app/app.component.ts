import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from './_services/auth.service';
import { TravelService } from './_services/travel.service';
import { AlertService } from "./_services/alert.service";

@Component({
  selector: 'my-app',
  templateUrl: './app.component.html',
  providers: [TranslateService]
})
export class AppComponent {

  constructor(private translate: TranslateService, private authService: AuthService, private alertService: AlertService) {
    translate.addLangs(["en", "fr"]);
    this.translate.setDefaultLang('en');
    this.translate.use('en');
  }

  ngOnInit(): void {
    // this.authService.init().subscribe(data => {

    // },
    // error => {

    // });
  }

  content() { }

  changeLanguage($event, lang: string) {
    $event.preventDefault();
    const allowedLanguages = ['en', 'nl', 'fr'];
    if (allowedLanguages.indexOf(lang) > -1) {
      this.translate.use(lang);
    } else {
      console.warn(`requested language '${lang}' not supported!`);
    }
  }
}


