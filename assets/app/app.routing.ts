import { Routes, RouterModule } from "@angular/router";
import { AuthenticationComponent } from "./auth/authentication.component";
import { TravelsComponent } from "./travel/travels.component";
import { UserComponent } from "./user/user.component"
import { PageNotFoundComponent } from "./pageNotFound/not-found.component"
import { UnauthorizedComponent } from "./unauthorized/unauthorized.component"

// Prevent login access
import { AuthGuard } from "./_guards/auth.guard";

import { AUTH_ROUTES } from "./auth/auth.routes";
import { USER_ROUTES } from "./user/user.routes";

const APP_ROUTES: Routes = [
    { path: '', redirectTo: 'auth', pathMatch: 'full' },
    { path: 'auth', component: AuthenticationComponent, children: AUTH_ROUTES },
    { path: 'user', component: UserComponent, children: USER_ROUTES },
    { path: 'travels', component: TravelsComponent, canActivate: [AuthGuard]},
    { path: 'unauthorized', component: UnauthorizedComponent },
    { path: '**', component: PageNotFoundComponent }
];

export const routing = RouterModule.forRoot(APP_ROUTES);