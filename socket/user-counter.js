module.exports = function(io){

    let numberUsers = 0;

    io.on('connection', function(socket){
        io.emit('user count', ++numberUsers);
        socket.on('disconnect', function(socket){
            io.emit('user count', --numberUsers);
        });
    });

}