var LocalStrategy = require("passport-local").Strategy;
var User = require("../models/user");
var Token = require("../models/token");
var bCrypt = require("bcrypt");
const crypto = require("crypto");
const nodemailer = require("../nodemailer");

module.exports = function(passport) {
  passport.use(
    "signup",
    new LocalStrategy(
      {
        usernameField: "email",
        passwordField: "password",
        passReqToCallback: true
      },
      function(req, username, password, done) {

        if (!username || !password) return done(null, false);

        findOrCreateUser = function() {
          // find a user in Mongo with provided email
          User.findOne({ "emails.value": username }, function(err, user) {
            // In case of any error, return using the done method
            if (err) {
              return done(err);
            }
            // already exists
            if (user) {
              // console.log("User already exists with this email: " + username);
              return done(null, false, "User already exists with this email: " + username);
            } else {
              // if there is no user with that email
              // create the user

              var newUser = new User({
                provider: "local",
                displayName: req.body.firstName + " " + req.body.lastName,
                name: {
                  familyName: req.body.lastName,
                  givenName: req.body.firstName,
                  middleName: ""
                },
                emails: [
                  {
                    value: username,
                    type: "local"
                  }
                ],
                password: createHash(password)
              });

              // save the user
              newUser.save(function(err) {
                if (err) {
                  return done(null, false);
                }


                var token = new Token({
                  _userId: newUser._id,
                  token: crypto.randomBytes(64).toString("hex")
                });

                token.save(function(err) {
                  if (err) {
                    return done(null, false);
                  }

                  var mailOptions = {
                    from: process.env.EMAIL_USERNAME || "no-reply@yourwebapplication.com",
                    to: newUser.emails[0].value,
                    subject: "Account Verification on TripTrack !",
                    text:
                      "Hello,\n\n" +
                      "Please verify your account by clicking the link: \nhttps://" +
                      req.headers.host +
                      "/user/verify/confirm/" +
                      token.token +
                      "\n"
                  };

                  nodemailer.transporter.sendMail(mailOptions, (err, info) => {
                    if (err) {
                      return done(err.message);
                    }

                    if (process.env.NODE_ENV === "development") {
                      console.log("Message sent: %s", info.messageId);
                      // Preview only available when sending through an Ethereal account
                      console.log(
                        "Preview URL: %s",
                        nodemailer.nodemailer.getTestMessageUrl(info)
                      );
                    }

                    // When user created, token created and verification mail send, back to the routes
                    return done(null, newUser);
                  });
                });
              });
            }
          });
        };

        // Delay the execution of findOrCreateUser and execute the method
        // in the next tick of the event loop
        process.nextTick(findOrCreateUser);
      }
    )
  );

  // Generates hash using bCrypt
  var createHash = function(password) {
    return bCrypt.hashSync(password, 10);
  };

  var sendVerificationEmail = function() {};
};
