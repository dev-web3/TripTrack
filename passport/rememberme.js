var RememberMeStrategy = require("passport-remember-me").Strategy;

var RememberMeToken = require("../models/rememberMeToken");
var User = require("../models/user");

module.exports = function(passport) {
  passport.use(
    new RememberMeStrategy(
      function(token, done) {
        RememberMeToken.consumeRememberMeToken(token, function(err, user) {
          if (err) {
            return done(err);
          }
          if (!user) {
            return done(null, false);
          }

          User.findOne({'_id': user}, function(err, user){
              if(err){
                  return done(err);
              }
              return done(null, user);
          });
        });
      },

      function(user, done) {
        //console.log("ici" + user);
        // var token = new RememberMeToken({
        //   _userId: user._id,
        //   token: crypto.randomBytes(64).toString("hex")
        // });

        // console.log(token);
        // RememberMeToken.issueToken(user, function(err, token){
        //   if(err){
        //     return done(err);
        //   }
        //   return done(null, token);
        // });
        
        // token.save(function(err) {
        //   console.log(err);
        //   if (err) {
        //     return done(err);
        //   }
        //   return done(null, token);
        // });
        return done(null, null);
      }
    )
  );
};
