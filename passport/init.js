var login = require('./login');
var signup = require('./signup');
var facebook = require('./facebook');
var google = require('./google');
var rememberme = require('./rememberme');
var User = require('../models/user');

module.exports = function(passport){

    // Setting up Passport Strategies for Login and SignUp/Registration
    login(passport);
    signup(passport)
    facebook(passport);
    google(passport);
    rememberme(passport);


    // Passport needs to be able to serialize and deserialize users to support persistent login sessions
    passport.serializeUser(function(user, done) {
        // console.log('serializing user: ');
        // console.log(user);
        done(null, user._id);
    });

    // used to deserialize the user
    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });
  
}
