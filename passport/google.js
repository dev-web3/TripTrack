var GoogleStrategy = require("passport-google-oauth").OAuth2Strategy;
var User = require("../models/user");
var OAuthConfig = require("../OAuth-config");

module.exports = function (passport) {
  passport.use(
    "google",
    new GoogleStrategy(
      {
        clientID: OAuthConfig.google.appID,
        clientSecret: OAuthConfig.google.appSecret,
        callbackURL: OAuthConfig.google.callbackUrl
      },

      function (accessToken, refreshToken, profile, done) {
        // console.log("[GOOGLE] profile: ", profile);

        // asynchronous
        process.nextTick(function () {
          // find the user in the database based on their facebook id
          User.findOne({ "google.id": profile.id }, function (err, user) {
            // if there is an error, stop everything and return that
            // ie an error connecting to the database
            if (err) return done(err);

            // if the user is found, then log them in
            if (user) {
              return done(null, user); // user found, return that user
            } else {
              // if there is no user found with that facebook id, create them
              var newUser = new User({
                provider: 'google',
                displayName: profile.displayName,
                name: {
                  familyName: profile.name.familyName,
                  givenName: profile.name.givenName,
                },
                emails: [
                  {
                    value: profile.emails[0].value,
                    type: 'google'
                  }
                ],
                password: '',
                google: {
                  id: profile.id,
                  token: accessToken
                },
                isVerified: true,
                //photos: profile.photos
              });

              // console.log(newUser);

              // save our user to the database
              newUser.save(function (err) {
                if (err) throw err;

                // if successful, return the new user
                return done(null, newUser);
              });
            }
          });
        });
      }
    )
  );
};
